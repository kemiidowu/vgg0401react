import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Nav from "./Component/Nav";
import Header from "./Component/Header";
import Sidebar from "./Component/Sidebar";
import Vueimg from "./Component/images/vue.png";
import Bootimg from "./Component/images/bootstrap.jpg"; 
import Reactimg from "./Component/images/react.png"; 
import Footer from "./Component/Footer"; 
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
  <div>
    <Nav />
    <Header />

    <div     
      style={{
        width: "100%",
        display: "flex",
        margin: '3rem 0'
      }}
    >
      <div>
        <Sidebar />
      </div>
      <div
        style={{
        //   flex: "0 0 10%"
        display: 'inline-block',
        margin: '.5rem'
        
        }}
      >
            <h2>  lorem lorem lorem</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse volutpat aliquet ex in tincidunt. Quisque ut lacinia felis, at rhoncus sapien. In rhoncus felis nec odio pulvinar varius. Sed ut convallis dui. Pellentesque ac sapien augue. Proin rutrum ante elit. Sed urna dolor, sodales venenatis sem nec, ultricies posuere sapien. Nunc gravida, metus id ullamcorper vestibulum, purus risus pharetra lectus, ac fermentum arcu tellus in libero. Integer in sapien quam. Cras commodo facilisis massa nec tristique. Etiam ornare leo id nulla sagittis lacinia sed sed urna. Suspendisse congue dui tellus, et lobortis turpis malesuada eu. Phasellus fringilla accumsan laoreet.</p>
            <div style= {{
                    display: 'grid',
                    gridTemplateRows: '200px 200px',
                    gridTemplateColumns: 'repeat(2,1fr)',
                    gridRowGap: '20px',
                    gridColumnGap: '30px',
                    padding: '1rem 0'
                    
            }}>
                <div className="grid-one">
                  <img src ={Vueimg} alt= "vue" width="200px" height="90vh" />   
                  <h3>Investment Planning</h3>
                  <p>The financial markets generally are unpredictable. So that one has to have different scenerios... </p>
                </div> 
                <div className="grid-one">
                  <img src ={Bootimg} alt= "Bootstrap" width="200px" height="90vh" />   
                  <h3>Investment Planning</h3>
                  <p>The financial markets generally are unpredictable. So that one has to have different scenerios... </p>
                </div>
                <div className="grid-one"> 
                <img src ={Reactimg} alt= "react" width="200px" height="90vh" /> 
                  <h3>Investment Planning</h3>
                  <p>The financial markets generally are unpredictable. So that one has to have different scenerios... </p> 
                </div> 
                <div className="grid-one">
                  <img src ={Vueimg} alt= "vue" width="200px" height="90vh" />

                  <h3>Investment Planning</h3>
                  <p>The financial markets generally are unpredictable. So that one has to have different scenerios... </p>
                </div>

            </div>
  
        </div>
    </div>
            <Footer />

  </div>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
