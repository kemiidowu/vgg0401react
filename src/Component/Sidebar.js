import React from 'react';

function Side() {
    return (
        <div style={{
            border: '.2rem solid  rgb(127, 127, 216)',
            flex: '0 0 40%',
            height: '34rem',
            position:'sticky',
            padding: '2rem',
            marginTop: '2rem',
            margin: '2rem'
        }}>
            <h2> My Search: </h2>
            <input type= "text" placeholder = "search for a keyword" height="1.5rem" ></input>
        </div>
    );
  }
  
  export default Side;