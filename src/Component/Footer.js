import React from 'react';
import './footer.css';

function Footer() {
  return (
        <footer >
            <div  style={{
                display: 'flex',
                textDecoration:'none',
                justifyContent: 'space-around'
                
            }}>

                <div>
                    <h4>Featured Job</h4>
                    <ul>

                        <li>Manage Responses</li>
                        <li>Report a Problem</li>
                        <li>Mobile Site</li>
                        <li>Jobs by Skill</li>
                    </ul>
                </div>
                
                <div >      
                <h4>Latest Job</h4>
                    <ul>

                        <li>Access Database</li>
                        <li>Manage Responses</li>
                        <li>Report a Problem</li>
                        <li>Mobile Site</li>
                        <li>Jobs by Skill</li>
                    </ul>
                </div>

    
            </div>
        <div class="container-fluid">
                    <div class="copy-right ">
                        <p>&copy;Copyright 2019 | Design By  KemTech</p>
                    </div>
        </div>
    </footer>
  );
}

export default Footer;
