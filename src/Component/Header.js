import React from 'react';
import './Header.css';
import Kemimg from '../Component/images/firstimg.jpeg';


function Header() {
    return (
      <div>
        <img src={Kemimg} alt="header" width="100%" height="520vh" />
        <h1 style ={{
          textAlign: 'center',
          fontSize: '1.3rem',
          marginTop: '2rem',
          fontWeight: '400'
        }}>We Are The Future </h1>
      </div>
    );
  }
  
  export default Header;